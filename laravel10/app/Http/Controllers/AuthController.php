<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailAuth;

class AuthController extends Controller
{
    public function index() {
        return view('halaman_auth.login');
    }

    public function login(Request $request) {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ], [
            'email.required' => 'Email wajib diisi',
            'password.required' => 'Password wajib diisi',
        ]);

        $infologin = [
            'email' => $request['email'],
            'password' => $request['password']
        ];

        if (Auth::attempt($infologin)) {
            if(Auth::user()->email_verified_at != null) {
                if(Auth::user()->role === 'admin') {
                    return redirect('/admin')->with('success', 'Halo admin anda berhasil login');
                } else if(Auth::user()->role === 'user') {
                    return redirect('/user')->with('success', 'Halo user anda berhasil login');
                }
            } else {
                Auth::logout();
                return redirect('/login')->withErrors('Akun anda belum aktif');
            }            
        } else {
            return redirect('/login')->withErrors('Email atau password salah');
        }
    }

    public function create() {
        return view('halaman_auth.register');
    }

    public function register(Request $request) {
        $str = Str::random(100);

        $request->validate([
            'fullname' => 'required|min:5',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6',
            'gambar' => 'required|image|file',
        ], [
            'fullname.required' => "Full Name wajib diisi",
            'fullname.min' => "Full Name minimal 5 karakter",
            'email.required' => "Email wajib diisi",
            'email.unique' => "Email telah terdaftar",
            'password.required' => "Password wajib diisi",
            'password.min' => "Password minimal 6 karakter",
            'gambar.required' => "Gambar wajib diupload",
            'gambar.image' => "Gambar yang diupload harus image",
            'gambar.file' => "Gambar harus berupa file",
        ]);

        $gambar_file = $request->file('gambar');
        $gambar_ekstensi = $gambar_file->extension();
        $nama_gambar = date('ymdhisv'). "." . $gambar_ekstensi;
        $gambar_file->move(public_path('picture/accounts'), $nama_gambar);

        $inforegister = [
            'fullname' => $request['fullname'],
            'email' => $request['email'],
            'password' => $request['password'],
            'gambar' => $nama_gambar,
            'verify_key' => $str
        ];

        User::create($inforegister);

        $details = [
            'name' => $inforegister['fullname'],
            'role' => 'user',
            'datetime' => date('Y-m-d H:i:s'),
            'website' => 'Laravel10 - SMTP',
            'url' => 'http://'. request()->getHttpHost(). "/". "verify/". $inforegister['verify_key'],
        ];

        Mail::to($inforegister['email'])->send(new MailAuth($details));

        return redirect('/login')->with('success', 'Link verifikasi telah dikirim ke email anda');
    }

    function verify($verify_key) {
        $keyCheck = User::select('verify_key')
        ->where('verify_key', $verify_key)
        ->exists();

        if($keyCheck) {
            $user = User::where('verify_key', $verify_key)->update(['email_verified_at' => date('Y-m-d H:i:s')]);

            return redirect('/login')->with('success', 'Verifikasi berhasil. Akun anda sudah aktif');
        } else {
            return redirect('/login')->withErrors('Key tidak valid')->withInput();
        }
    }

    function logout(){
        Auth::logout();
        return redirect('/');
    }
}
